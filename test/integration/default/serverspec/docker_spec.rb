require 'serverspec'

set :backend, :exec

describe command('/usr/bin/docker network ls') do
  its(:exit_status) { should eq 0 }
  its(:stdout) { should_not include("docker_net")}
  its(:stdout) { should include("ecmc-multi-host-network")}
end

describe file('/etc/default/docker') do
  it { should exist }
  it { should_not contain("http_proxy") }
  it { should_not contain("https_proxy") }
end
