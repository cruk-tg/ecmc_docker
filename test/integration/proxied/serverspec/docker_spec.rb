require 'serverspec'

set :backend, :exec

describe command('/usr/bin/docker network ls') do
  its(:exit_status) { should eq 0 }
  its(:stdout) { should_not include("docker_net")}
end

describe file('/etc/default/docker') do
  it { should exist }
  it { should contain("http_proxy") }
  it { should contain("https_proxy") }
end
