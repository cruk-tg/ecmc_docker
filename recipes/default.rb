#
# Cookbook Name:: ecmc_docker
# Recipe:: default
#
# Copyright 2018, The University of Edinburgh
#
# All rights reserved - Do Not Redistribute
#

firewall_rule 'Docker_Swarm_cluster_management' do
  port 2377
  protocol :tcp
  source '192.168.99.0/24'
  command :allow
end

firewall_rule 'Docker_Internode_comms_tcp' do
  port 7946
  protocol :tcp
  source '192.168.99.0/24'
  command :allow
end

firewall_rule 'Docker_Internet_comms_udp' do
  port 7946
  protocol :udp
  source '192.168.99.0/24'
  command :allow
end

firewall_rule 'Docker_Network_Traffic_overlay' do
  port 4789
  protocol :udp
  source '192.168.99.0/24'
  command :allow
end

docker_service 'default' do
  http_proxy node['http_proxy']
  https_proxy node['https_proxy']
  no_proxy "localhost,#{node['ipaddress']},vcs.ecmc.ed.ac.uk"
  action [:create, :start]
end

docker_network 'ecmc-multi-host-network' do
  subnet '192.168.88.0/24'
  gateway '192.168.88.1'
  action :create
end
