2.0.0
=====
Removes requirement for ETCD

ecmc_docker Cookbook
====================
Creates default docker firewall rules for 172.19.158.0/23
Creates default docker firewall rules for 129.215.158.0/23

License and Authors
-------------------
Authors: Paul D Mitchell <paul.d.mitchell@ed.ac.uk>
