name             'ecmc_docker'
maintainer       'Paul D Mitchell'
maintainer_email 'paul.d.mitchell@ed.ac.uk'
license          'All rights reserved'
description      'Installs/Configures ecmc_docker'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '3.1.0'

depends 'docker', '~> 4.4.1'
depends 'firewall'
